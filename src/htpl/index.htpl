@extends('layout.htpl')

@section('header')
    <link rel="stylesheet" href="css/chart.css">
    <link rel="stylesheet" href="css/time-chart.css">
    <link rel="stylesheet" href="css/index.css">
    <title>SARS-CoV-2 N vs Time</title>
@stop

    @section('content')
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-offset-1">

        <div>
          <div class="row">
            <h1 class='en'><select class="" id="type-sel-en"></select> vs Time</h1>
            <h1 class='jp'><select class="" id="type-sel-jp"></select> 対 時間</h1>
            <br/>
            <br/>
            <div class="row">
              <div class="col-xs-3">
                <span class='en'>X-axis scale </span>
                <span class='jp'>X 軸の目盛</span>
                
                <div class="btn-group" id="xscale">
                  <a class="btn btn-default btn-sm radio-selector" value="log">log</a>
                  <a class="btn btn-default btn-sm radio-selector" value="linear">linear</a>
                </div>
              </div>
              <div class="col-xs-3">
                <span class='en'>Y-axis scale </span>
                <span class='jp'>Y 軸の目盛</span>

                <div class="btn-group" id="yscale">
                  <a class="btn btn-default btn-sm radio-selector" value="log">log</a>
                  <a class="btn btn-default btn-sm radio-selector" value="linear">linear</a>
                </div>
              </div>
              <div class="col-xs-3 pull-right">
                <div class="btn-group" id="lang">
                  <a class="btn btn-default btn-sm radio-selector" value="en">English</a>
                  <a class="btn btn-default btn-sm radio-selector" value="jp">日本語</a>
                </div>
              </div>
            </div>
            <br/>
            <div class="row">
              <div class="col-xs-4">
                <span class='en'>reference0</span>
                <span class='jp'>参照国 0</span>
                <select class="btn btn-default btn-sm" id="ref0-sel"></select>
              </div>
              <div class="col-xs-4 col-xs-offset1">
                <span class='en'>reference1</span>
                <span class='jp'>参照国 1</span>
                <select class="btn btn-default btn-sm" id="ref1-sel"></select>
              </div>
              <div class="col-xs-4 col-xs-offset1">
                <span class='en'>reference2</span>
                <span class='jp'>参照国 2</span>
                <select class="btn btn-default btn-sm" id="ref2-sel"></select>
              </div>
            </div>

            <div class="row">
              <div id='chart0'></div>
            </div>

            <div class="row">
              <div class="col-sm-9 col-sm-offset-1">
                <div class='en'>
                  <h4>Description</h4>
                  <ul>
                    <li>In order to see if the number of carriers is growing "exponentially", you can use linear scale for X axis and logarithmic scale for Y axis.
                      By doing so, an exponential growth is represented by a straight line and you can clearly see the discrepancy of the measured values from that line.
                    <li>On the other hand, a plot using logarithmic scale for both X and Y is also useful. In that case, a growth proportional to days, days to the 2nd power, days to the 3rd power, ... or days to the N-th power, are all represented by straight lines.
                  </ul>
                  <h4>Data sources</h4>
                  <ul>
                    <li><a href='https://ourworldindata.org/coronavirus-source-data'>Coronavirus Source Data</a>
                    <li><a href='https://raw.githubusercontent.com/kaz-ogiwara/covid19/master/data/data.json' class='en'>Toyo Keizai GitHub</a>
                  </ul>
                  <h4>Author</h4>
                  Atsushi Kawai (atsushi at xf7.so-net.ne.jp)
                  <h4>License</h4>
                  MIT License
                  <h4>Source code</h4>
                  Everything is downloaded and running on your web browser. Nothing is running on the backend server. Feel free to hack.
                </div>
                <div class='jp'>
                  <h4>説明</h4>
                  <ul>
                    <li>罹患者数が「指数関数的」に増えているかどうかを見るには X 軸の目盛を線形 (linear) に、Y 軸の目盛を対数 (log) にすると良いかも。
                      そういう目盛でプロットすると指数関数は直線になるので、そこからどれくらいズレているかひとめで分かるかも。
                    <li>X 軸も Y 軸も対数にした場合は、日数に比例する増え方や、日数の 2 乗に比例する増え方、3 乗に比例する増え方、一般に N 乗に比例する増え方が
                      直線で表されるので、そういうのと比べたいときには便利かも。
                    <li>X 軸だけを対数にして Y 軸は線形、というプロットは「対数関数的」に増えているかどうかを見るには良いかも。そんな増え方する事例があるのかどうかは<a href='loggrowth.jpg'>知りません</a>。
                  </ul>
                  <h4>データの取得元</h4>
                  <ul>
                    <li><a href='https://ourworldindata.org/coronavirus-source-data'>Coronavirus Source Data</a>
                    <li><a href='https://raw.githubusercontent.com/kaz-ogiwara/covid19/master/data/data.json' class='jp'>東洋経済の中の人の GitHub</a>
                  </ul>
                  <h4>作者</h4>
                  川井 敦 (atsushi at xf7.so-net.ne.jp)
                  <h4>利用許諾</h4>
                  MIT ライセンス
                  <h4>ソースコード</h4>
                  サーバ側で動いているコードはありません。Web ブラウザにダウンロードされているものですべてです。好きにしてよいよいよい (残響音含む)。
                </div>
                <footer>
                  <a href='/'>go to site top</a>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop

@section('footer')
    <script src="js/index.js"></script>
    <script src="js/chart.js"></script>
    <script src="js/time-chart.js"></script>
@stop
