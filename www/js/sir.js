"use strict";

let SirModel = null;

!function($) {
    SirModel = function(params) {
        try {
            for (let key in params) {
                this[key] = params[key];
            }
            this.t = 0;
        }
        catch (e) {
            console.log(e);
        }
    };

    SirModel.prototype = {
        constructor: SirModel,

        reload_params: function(params) {
            if (typeof params !== 'undefined') {
                for (let key in params) {
                    this[key] = params[key];
                }
            }
        },

        result: {
            step: [],
        },

        init: function() {
            this.t = 0.0;
            this.result.step = [];
        },

        integrate: function() {
            let s0 = this.s, i0 = this.i, r0 = this.r;
            let nbeta = this.nbeta;
            let gamma = 1.0 / this.gammainv;
            let dt = this.dt;
            let n = this.n;
            let i1 = i0 + (nbeta * (1 - i0 - r0) - gamma) * i0 * dt;
            let r1 = r0 + gamma * i0 * dt;

            // console.log('n:', n, ' i1:', i1, ' i1 n:', i1 * n);
            let s1 = 1 - i1 - r1;

            this.s = s1;
            this.i = i1;
            this.r = r1;
            this.t += dt;
            this.result.step.push(
                {
                    t: this.t,
                    s: this.s,
                    i: this.i,
                    r: this.r,
                }
            );
        },
        report: function() {
            return this.result;
        },
    };
}(jQuery);
