"use strict";

let TimeChart = null;

!function($) {
    TimeChart = function(containerstr, params) {
        Chart.call(this, containerstr, params);
        this.container.className += " time-chart";
        this.time0 = Date.parse(this.date0);

        // initial plot                                                                                  
        this.new_plot();
    };

    TimeChart.prototype = Object.create(Chart.prototype);

    let prototype = {
        constructor: TimeChart,

        parse_date: d3.timeParse('%Y/%m/%d'),

        datestr2days: function(dstr) {
            let me = this;
            let date = me.parse_date(dstr);
            return (date.getTime() - me.time0) / 1000 / 3600 / 24; // days since time0
        },

        days2datestr_gen: function() {
            let me = this;
            return function(days) {
                let d = new Date(+days * 24 * 3600 * 1000 + me.time0);
                let dstr = My.date_to_string(d, '%b %d');
                return dstr;
            }
        },

        reload_params: function(params) {
            Chart.prototype.reload_params.call(this, params);
            this.time0 = Date.parse(this.date0);
        },

        load_data: function() {
            let me = this;
            let promise = this.loader(me).then(function(res){
                me.datatype = res.type;
                me.legends = res.legends;
                me.colors = res.colors;
                me.opacity = res.opacity;
                me.country = res.country;
                me.stroke_width = res.width;
                me.data = res.data;
                me.nplots = me.data[0].vals.length;
                let xmax = null;
                let ymax = null;
                for (let d of me.data) { // each day
                    for (let i = 0; i < d.vals.length; i++) {
                        let y = d.vals[i];
                        if (typeof y === 'function') continue;
                        if (y == null) continue;
                        // console.log('i:', i, ' pop:', me.country[i].population);
                        y = +y / me.country[i].population * 100000000;
                        if (ymax == null) ymax = y;
                        if (y > ymax) ymax = y;
                        // console.log(d.date, 'ymax:', ymax, ' y:', +y);
                    }
                }
                me.xdomain = null;
                me.ymax = ymax;
                me.ymin = me.ymin || 3;
                me.ydomain = [me.ymin, me.ymax * 1.1];
            });
            return promise;
        },

        plot_d3: function() {
            let me = this;
            let divid = me.container.str;
            d3.select(divid).select("svg").remove();

            let svg = d3.select(divid).append("svg")
                .attr("width", me.width)
                .attr("height",me.height)
                .append("g")
                .attr("class", "base")
                .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

            me.scale_xy();

            let fmt = d3.formatSpecifier('d');
            fmt.comma = true;
            me.xaxis = d3.axisBottom()
                .scale(me.xscale)
                .ticks(10)
                .tickValues(me.xtickvals())
                .tickFormat(me.days2datestr_gen())
            me.yaxis = d3.axisLeft()
                .scale(me.yscale)
                // .tickValues(tvals)
                .ticks(5)
                .tickFormat(d3.format(fmt))

            function make_y_gridlines() {
                return d3.axisLeft(me.yscale).ticks(5)
            }

            me.line = [];
            for (let p = 0; p < me.nplots; p++) {
                me.line[p] = d3.line()
                    .x(function(d) {
                        let x = me.datestr2days(d.date);
                        // console.log('x:', x, ' xscale:', me.xscale(x));
                        return me.xscale(x);
                    })
                    .y(function(d, i) {
                        if (typeof d.vals[p] === 'function') {
                            let x = me.datestr2days(d.date);
                            return me.yscale(d.vals[p](x));
                        }
                        else {
                            let xoff = +me.country[p].offset[me.datatype];
                            let y = +me.data[i + xoff].vals[p] / me.country[p].population * 100000000;
                            // console.log('y:', y, ' yscale:', me.yscale(y));
                            return me.yscale(y);
                        }
                    })
                    .defined(function(d, i) {
                        let x = me.datestr2days(d.date);
                        if (x < 1) return false;
                        if (me.country[p] && me.data.length <= i + +me.country[p].offset[me.datatype]) return false;
                        if (typeof d.vals[p] === 'function') {
                            return x > 0;
                        }
                        else {
                            let xoff = +me.country[p].offset[me.datatype];
                            if (i + xoff < 0 || i + xoff >= me.data.length) return false;
                            let y = +me.data[i + xoff].vals[p] / me.country[p].population * 100000000;
                            return y > 0;
                            // return me.yscale(y) > 0;
                        }
                    });

            }

            // data plot                                                                                         
            let clippath = svg.append("clipPath")
                .attr("id", "clip0")
                .append("rect")
                .attr("id", "rect0")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", me.width0)
                .attr("height", me.height0)

            for (let j = 0; j < me.nplots; j++) {
                let st = me.colors[j % me.colors.length];
                let op = me.opacity[j % me.opacity.length];
                let sw = me.stroke_width[j % me.stroke_width.length];
                svg.append("g")
                    .attr("clip-path", "url(#clip0)")
                    .append("path")
                    .datum(me.data)
                    .attr("stroke", st)
                    .attr("stroke-width", sw)
                    .attr("opacity", op)
                    .attr("class", "line")
                    .attr("id", "line" + j)
                    .attr("d", me.line[j]);
            }
            svg.append("g")
                .attr("class", "grid")
                .call(make_y_gridlines().tickSize(-me.width0).tickFormat(""))

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0,  " + me.height0 + ")")
                .call(me.xaxis)
                .selectAll("text")
                .style("font-size", me.fontsize1)
                .style("text-anchor", "center")
                .attr("transform", "rotate(-20)");

            svg.append("g")
                .attr("class", "y axis")
                .call(me.yaxis)
                .selectAll("text")
                .style("font-size", me.fontsize1);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "ylabel")
                .attr("x", -me.height0 / 2.0)
                .attr("y", -40)
                .attr("text-anchor", "middle")
                .attr("transform", "rotate(-90)")
                .text(me.ylabel);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "xlabel")
                .attr("x", me.width0 / 2.0)
                .attr("y",  me.height0 + 40)
                .attr("text-anchor", "middle")
                .text(me.xlabel);

            for (let i = 0; i < me.nplots; i++) {
                let id = 'legend' + i;
                let x = me.width0 - 200;
                let y = me.margin.top + me.height0 + 25 * i - 300;
                if (i >= 10) {
                    x += 40;
                    y -= 25 * 6;
                }
                svg.append("g")
                    .append("text")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("id", id)
                    .attr("class", 'legend')
                    .style("text-anchor", "left")
                    .style("font-size", me.fontsize0)
                    .attr("stroke", me.colors[i])
                    .attr("fill", me.colors[i])
                    .attr("opacity", me.opacity[i])
                    .text(me.legends[i]);
            }
        },

        replot_d3: function() {
            let me = this;
            let divid = me.container.str;
            let svg = d3.select(divid).selectAll(".base");

            me.scale_xy();

            me.xaxis.scale(me.xscale).ticks(10);
            me.yaxis.scale(me.yscale).ticks(5);

            svg.selectAll("g.x")
                .transition().duration(400)
                .call(me.xaxis)
                .selectAll("text")
                .attr("transform", "rotate(-20)")

            svg.selectAll("g.y")
                .transition().duration(400)
                .call(me.yaxis);

            for (let j = 0; j < me.nplots; j++) {
                let d = svg.selectAll("path#" + 'line' + j).datum(me.data);
                d.transition().duration(400).attr("d", me.line[j]);
            }

            svg.selectAll(".guideLine").remove();
            svg.selectAll(".tip").remove();

            d3.select(divid).on("mousemove", function() {
                let area = document.getElementById("chart0");
                let pos = d3.mouse(area);
                let ml = me.margin.left;
                let time = me.xscale.invert(pos[0] - ml);
                let i = time2index(time);
                time = time.toFixed(1);
                if (i <= 0 || me.data.length <= i) return;
                let p = me.data;
                let days = me.datestr2days(p[i].date);
                let val = p[i].vals[0];
                val = isNaN(val) ? 'N/A' : val;
                guideLine
                    .transition()
                    .duration(10)
                    .attr("x1", pos[0] - ml * 2)
                    .attr("x2", pos[0] - ml * 2)
                tooltip
                    .attr("x", pos[0] - ml - 20)
                    .text(me.days2datestr_gen()(days))
                tooltip2
                    .attr("x", pos[0] - ml - 20)
                    .text('cases:' + val)

                function time2index(t) {
                    let p = me.data;
                    let i = 0;
                    while (true) {
                        if (i >= p.length) break;
                        let t0 = me.datestr2days(p[i].date);
                        if (t0 >= time) break;
                        i++;
                    }
                    return i;
                }
            });

            let overLayer = svg.append("g").attr("class", "overLayer")
                .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

            let guideLine = overLayer.append("line")
                .attr("class", "guideLine")
                .attr("x1", 0)
                .attr("y1", -me.margin.top + 50)
                .attr("x2", 0)
                .attr("y2", -me.margin.top + me.height0)
                .attr("stroke", "#aaaaaa")

            let tooltip = svg.append("g").append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "tip")
                .attr("x", 0)
                .attr("y",  me.margin.top - 20)
                .attr("text-anchor", "left")
                .text("");

            let tooltip2 = svg.append("g").append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "tip")
                .attr("x", 0)
                .attr("y",  me.margin.top)
                .attr("text-anchor", "left")
                .text("");

        },

        scale_xy: function() {
            let me = this;
            let d = me.data;
            let d0 = new Date(Date.parse(me.date0) + 1000 * 3600 * 24 * 9);
            d0 = me.datestr2days(My.date_to_string(d0, '%Y/%m/%d'));
            let d1 = me.datestr2days(d[d.length - 1].date) + 5;
            me.xscale = me.islogx ? d3.scaleLog() : d3.scaleLinear();
            me.xscale.domain([d0, d1]).range([0, me.width0]);
            let yd = [me.ydomain[0], me.ydomain[1]];
            me.yscale = me.islogy ? d3.scaleLog() : d3.scaleLinear();
            me.yscale.domain(yd).range([me.height0, me.margin.top]);
        },

        xtickvals: function() {
            let me = this;
            let ticks = [];

            if (me.islogx) {
                for (let i in me.data) {
                    if (i % 7) continue;
                    let d = me.data[i];
                    let days = me.datestr2days(d.date);
                    if (days > 0) ticks.push(days);
                }
            }
            else {
                for (let i in me.data) {
                    if (i % 7) continue;
                    let d = me.data[i];
                    ticks.push(me.datestr2days(d.date));
                }
            }
            return ticks;
        },
    }

    Object.assign(TimeChart.prototype, prototype);

}(jQuery);
