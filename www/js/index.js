!function($) {
    "use strict";

    var DataType = 0; // 0: cases 1: deaths
    let ChartId = '#chart0';
    let ChartInstance = null;
    let Cases = [];
    let DefaultXscale = 'log';
    let DefaultYscale = 'log';
    let DefaultLang = 'en';
    let IsEnglish = null;

    function init_view() {

        // x scaling radio buttons
        let $xsradio = $('#xscale');
        $xsradio.find('.btn[value=' + DefaultXscale + ']').addClass('active');
        $xsradio.find('.btn').click(function() {
            $xsradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            ChartInstance.replot({
                islogx: $(this).attr('value') === 'log',
            });
        });

        // y scaling radio buttons
        let $ysradio = $('#yscale');
        $ysradio.find('.btn[value=' + DefaultYscale + ']').addClass('active');
        $ysradio.find('.btn').click(function() {
            $ysradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            ChartInstance.replot({
                islogy: $(this).attr('value') === 'log',
            });
        });

        // language radio buttons
        let xlabel_jp = '日付';
        let ylabel_jp = [ 'N: 人口 1 億人あたりの罹患者数', 'N: 人口 1 億人あたりの死亡者数' ];
        let xlabel_en = 'date';
        let ylabel_en = [ 'Number of cases per 100 million', 'Number of deaths per 100 million' ];
        let $langradio = $('#lang');
        $langradio.find('.btn[value=' + DefaultLang + ']').addClass('active');
        IsEnglish = DefaultLang === 'en' ? true : false;
        My.switch_lang(IsEnglish);
        $langradio.find('.btn').click(function() {
            $langradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            IsEnglish = $(this).attr('value') === 'en' ? true : false;
            My.switch_lang(IsEnglish);
            ChartInstance.replot({
                xlabel: IsEnglish ? xlabel_en : xlabel_jp,
                ylabel: IsEnglish ? ylabel_en[DataType] : ylabel_jp[DataType],
            });
            ChartInstance.new_plot();
        });

        // data type
        $('#type-sel-en').append($("<option>").text('Cases').val(0))
        $('#type-sel-jp').append($("<option>").text('罹患者数').val(0))
        $('#type-sel-en').append($("<option>").text('Deaths').val(1))
        $('#type-sel-jp').append($("<option>").text('死者数').val(1))
        $('#type-sel-en').change(function() {
            change_datatype($(this).val())
        });
        $('#type-sel-jp').change(function() {
            change_datatype($(this).val())
        });

        function change_datatype(type) {
            DataType = type;
            $('#type-sel-en').val(type);
            $('#type-sel-jp').val(type);
            let param = {
                datatype: type,
                date0: type == 0 ? '2020/02/01' : '2020/02/29',
                xlabel: IsEnglish ? xlabel_en : xlabel_jp,
                ylabel: IsEnglish ? ylabel_en[DataType] : ylabel_jp[DataType],
            };
            My.init_model('ow', type).then(function(raw) {
                Cases = My.raw2analyzed_ow(raw, +25);
                ChartInstance.reload_params(param);
                ChartInstance.new_plot();
            });
        }

        // reference country0
        for (let sel of [ '#ref0-sel', '#ref1-sel', '#ref2-sel', ]) {
            for (let c in My.country) {
                if (c === 'Japan') continue;
                let text = c;
                $(sel).append($("<option>").text(text).val(c))
            }
            $(sel).change(function() {
                ChartInstance.new_plot();
            });
        }
        $('#ref0-sel').val('World w/o China');
        $('#ref1-sel').val('China');
        $('#ref2-sel').val('United States');

        let dparam = {
            width: 768,
            height: 768,
            margin: { top: 50, right: 50, bottom: 50, left: 70, },
            loader: loader,
            date0: '2020/02/01',
            xlabel: IsEnglish ? xlabel_en : xlabel_jp,
            ylabel: IsEnglish ? ylabel_en[DataType] : ylabel_jp[DataType],
            islogx: DefaultXscale === 'log',
            islogy: DefaultYscale === 'log',
            lang: $('#lang').find('.btn.active').attr('value'),
            ymin: null,
        };
        ChartInstance = new TimeChart(ChartId, dparam);
    }

    function loader() {
        let $dfd = new $.Deferred();
        let data = [];

        for (let c of Cases) {
            if (!c.date) continue;
            var ref0 = $('#ref0-sel').val();
            var ref1 = $('#ref1-sel').val();
            var ref2 = $('#ref2-sel').val();
            data.push({
                date: c.date.replace(/-/g, '/'),
                vals: [
                    c['Japan'],
                    function(t){ let c = [ 6, 8 ]; let a = [ 0.1, 0.05 ]; return c[DataType] * Math.exp(a[DataType] * t); },
                    function(t){ let c = [ 0.0001, 0.00007 ]; return c[DataType] * t * t * t * t * t; },
                    function(t){ let c = [ 0.001, 0.0007 ]; return c[DataType] * t * t * t * t; },
                    function(t){ let c = [ 0.01, 0.008 ]; return c[DataType] * t * t * t; },
                    function(t){ let c = [ 0.12, 0.08 ]; return c[DataType] * t * t; },
                    function(t){ let c = [ 1.5, 0.8 ]; return c[DataType] * t; },
                    c[ref0],
                    c[ref1],
                    c[ref2],
                ],
            })
        }
        let ctr = My.country;
        let legend_en = [ 'Japan', 'exponential growth',
                          'days to the 5th',
                          '4th', '3rd', '2nd', 'proportional to days',
                          ref0 + ' ' + ctr[ref0].offset[DataType] + ' days',
                          ref1 + ' ' + ctr[ref1].offset[DataType] + ' days',
                          ref2 + ' ' + ctr[ref2].offset[DataType] + ' days',
                        ];

        let legend_jp = [ '日本 実測数', '指数関数的増加',
                          '日数の5乗に比例して増加', '4乗に比例', '3乗に比例', '2乗に比例', '日数に比例して増加',
                          ctr[ref0].jp + ' ' + ctr[ref0].offset[DataType] + '日',
                          ctr[ref1].jp + ' ' + ctr[ref1].offset[DataType] + '日',
                          ctr[ref2].jp + ' ' + ctr[ref2].offset[DataType] + '日',
                        ];
        let dst = {
            type: DataType,
            legends: IsEnglish ? legend_en: legend_jp ,
            colors: [ '#4444ff', '#8844ff', '#4488ff', '#44ccff', '#22dddd', '#22ffcc', '#88ff44', '#ccdd44', '#ee8844', '#dd2222', ],
            opacity: [ 1.0, 0.7, 0.7, 0.7, 0.7, 0.7, 0.7, 1.0, 1.0, 1.0, ],
            width: [ 3.0, 1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 3.0, 3.0, 3.0, ],
            country: [ctr.Japan, null, null, null, null, null, null, ctr[ref0], ctr[ref1], ctr[ref2], ],
            data: data,
        };
        $dfd.resolve(dst);
        return $dfd.promise();
    }

    $(function init() {
        window.My = new MyUtil();
        let p = My.get_url_param('lang');
        if (p) DefaultLang = p;
        My.set_lang(DefaultLang);
        My.init_model('ow', DataType).then(function(raw) {
            Cases = My.raw2analyzed_ow(raw, +25);
            init_view();
        });
        let timer = false;
        window.addEventListener('resize', function() {
            if (timer !== false) {
                clearTimeout(timer);
            }
            timer = setTimeout(function() {
                let c = ChartInstance;
                let w = $('#chart0').width();
                let m = c.margin;
                m.left = 512 < w ? 100 : 40;
                c.replot({ width: w, margin : m, });
            }, 1000);
        });

    });

}(jQuery);
