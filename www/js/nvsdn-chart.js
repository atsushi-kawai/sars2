"use strict";

let NvsdnChart = null;

!function($) {
    NvsdnChart = function(containerstr, params) {
        Chart.call(this, containerstr, params);
        this.container.className += " nvsdn-chart";

        // initial plot                                                                                  
        this.new_plot();
    };

    NvsdnChart.prototype = Object.create(Chart.prototype);

    let prototype = {
        constructor: NvsdnChart,

        load_data: function() {
            let me = this;
            let promise = this.loader(me).then(function(res){
                me.legends = res.legends;
                me.colors = res.colors;
                me.opacity = res.opacity;
                me.country = res.country;
                me.stroke_width = res.width;
                me.data = res.data;
                me.nplots = me.data[0].xvals.length;
                let xmax = null;
                let ymax = null;
                for (let d of me.data) { // each day
                    for (let i = 0; i < d.xvals.length; i++) {
                        let x = d.xvals[i];
                        let y = d.yvals[i];
                        let scale = 100000000 / me.country[i].population;
                        if (x != null) {
                            x = +x * scale;
                            if (xmax == null) xmax = x;
                            if (x > xmax) xmax = x;
                        }
                        if (y != null) {
                            y = +y * scale;
                            if (ymax == null) ymax = y;
                            if (y > ymax) ymax = y;
                        }
                    }
                }
                me.xmin = 1.0;
                me.xmax = xmax;
                me.xdomain = [me.xmin, me.xmax * 1.1];
                me.ymin = 1.0;
                me.ymax = ymax;
                me.ymin = me.ymin || 3;
                me.ydomain = [me.ymin, me.ymax * 1.1];
            });
            return promise;
        },

        plot_d3: function() {
            let me = this;
            let divid = me.container.str;
            d3.select(divid).select("svg").remove();

            let svg = d3.select(divid).append("svg")
                .attr("width", me.width)
                .attr("height",me.height)
                .append("g")
                .attr("class", "base")
                .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

            me.scale_xy();

            let fmt = d3.formatSpecifier('d');
            fmt.comma = true;
            me.xaxis = d3.axisBottom()
                .scale(me.xscale)
                .ticks(5)
                .tickFormat(d3.format(fmt))
            me.yaxis = d3.axisLeft()
                .scale(me.yscale)
                // .tickValues(tvals)
                .ticks(5)
                .tickFormat(d3.format(fmt))

            function make_y_gridlines() {
                return d3.axisLeft(me.yscale).ticks(5)
            }

            me.line = [];
            for (let p = 0; p < me.nplots; p++) {
                me.line[p] = d3.line()
                    .x(function(d, i) {
                        let x = +d.xvals[p] / me.country[p].population * 100000000;
                        return me.xscale(x);
                    })
                    .y(function(d, i) {
                        let y = +d.yvals[p] / me.country[p].population * 100000000;
                        return me.yscale(y);
                    })
                    .defined(function(d, i) {
                        return +d.yvals[p] > 0 && +d.xvals[p] > 0;
                    });
            }

            // data plot                                                                                         
            let clippath = svg.append("clipPath")
                .attr("id", "clip0")
                .append("rect")
                .attr("id", "rect0")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", me.width0)
                .attr("height", me.height0)

            for (let j = 0; j < me.nplots; j++) {
                let st = me.colors[j % me.colors.length];
                let op = me.opacity[j % me.opacity.length];
                let sw = me.stroke_width[j % me.stroke_width.length];
                svg.append("g")
                    .attr("clip-path", "url(#clip0)")
                    .append("path")
                    .datum(me.data)
                    .attr("stroke", st)
                    .attr("stroke-width", sw)
                    .attr("opacity", op)
                    .attr("class", "line")
                    .attr("id", "line" + j)
                    .attr("d", me.line[j]);
            }
            svg.append("g")
                .attr("class", "grid")
                .call(make_y_gridlines().tickSize(-me.width0).tickFormat(""))

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0,  " + me.height0 + ")")
                .call(me.xaxis)
                .selectAll("text")
                .style("font-size", me.fontsize1)
                .style("text-anchor", "center")
                .attr("transform", "rotate(-20)");

            svg.append("g")
                .attr("class", "y axis")
                .call(me.yaxis)
                .selectAll("text")
                .style("font-size", me.fontsize1);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "ylabel")
                .attr("x", -me.height0 / 2.0)
                .attr("y", -40)
                .attr("text-anchor", "middle")
                .attr("transform", "rotate(-90)")
                .text(me.ylabel);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "xlabel")
                .attr("x", me.width0 / 2.0)
                .attr("y",  me.height0 + 40)
                .attr("text-anchor", "middle")
                .text(me.xlabel);

            for (let i = 0; i < me.nplots; i++) {
                let id = 'legend' + i;
                let x = me.width0 - 600;
                let y = me.margin.top + me.height0 + 25 * i - 600;
                if (i >= 10) {
                    x += 40;
                    y -= 25 * 6;
                }
                svg.append("g")
                    .append("text")
                    .attr("x", x)
                    .attr("y", y)
                    .attr("id", id)
                    .attr("class", 'legend')
                    .style("text-anchor", "left")
                    .style("font-size", me.fontsize0)
                    .attr("stroke", me.colors[i])
                    .attr("fill", me.colors[i])
                    .attr("opacity", me.opacity[i])
                    .text(me.legends[i]);
            }

            for (let p = 0; p < me.nplots; p++) {
                let scale = 100000000 / me.country[p].population;
                let text = svg.selectAll(".datelabel" + p)
                    .data(me.data)
                    .enter()
                    .append("g")
                    .append("text")
                    .attr("class", 'datelabel' + p)
                    .style("text-anchor", "left")
                    .style("font-size", 10)
                    .attr("stroke", me.colors[p])
                    .attr("fill", me.colors[p])
                    .attr("opacity", me.opacity[p])
            }
        },


        replot_d3: function() {
            let me = this;
            let divid = me.container.str;
            let svg = d3.select(divid).selectAll(".base");

            me.scale_xy();

            me.xaxis.scale(me.xscale).ticks(5);
            me.yaxis.scale(me.yscale).ticks(5);

            svg.selectAll("g.x")
                .transition().duration(400)
                .call(me.xaxis)
                .selectAll("text")
                .attr("transform", "rotate(-20)")

            svg.selectAll("g.y")
                .transition().duration(400)
                .call(me.yaxis);

            for (let j = 0; j < me.nplots; j++) {
                let d = svg.selectAll("path#" + 'line' + j).datum(me.data);
                d.transition().duration(400).attr("d", me.line[j]);
            }

            for (let p = 0; p < me.nplots; p++) {
                let scale = 100000000 / me.country[p].population;
                let text = svg.selectAll(".datelabel" + p)
                    .data(me.data)
                    .transition().duration(400)
                    .attr("x", function(d, i) {
                        let ix = +d.xvals[p] * scale;
                        let x = me.xscale(ix);
                        if (!isFinite(x)) return 0;
                        return x;
                    })
                    .attr("y", function(d, i) {
                        let iy = +d.yvals[p] * scale;
                        let y = me.yscale(iy);
                        if (!isFinite(y)) return 0;
                        return y;
                    })
                    .text(function(d, i) {
                        let datestr = My.date_to_string(new Date(d.date), '%b %d');
                        let ix = +d.xvals[p] * scale;
                        let iy = +d.yvals[p] * scale;
                        return (ix > 1 && iy > 1 && i % 7 == 0) ? datestr : '';
                    });
            }
        },

        scale_xy: function() {
            let me = this;
            let d = me.data;

            let xd = [me.xdomain[0], me.xdomain[1]];
            me.xscale = me.islogx ? d3.scaleLog() : d3.scaleLinear();
            me.xscale.domain(xd).range([0, me.width0]);

            let yd = [me.ydomain[0], me.ydomain[1]];
            me.yscale = me.islogy ? d3.scaleLog() : d3.scaleLinear();
            me.yscale.domain(yd).range([me.height0, me.margin.top]);
        },
    }

    Object.assign(NvsdnChart.prototype, prototype);

}(jQuery);
