"use strict";

let SimChart = null;

!function($) {
    SimChart = function(containerstr, params) {
        Chart.call(this, containerstr, params);
        this.container.className += " sim-chart";

        // initial plot                                                                                  
        this.new_plot();
    };

    SimChart.prototype = Object.create(Chart.prototype);

    let prototype = {
        constructor: SimChart,

        parse_date: d3.timeParse('%Y/%m/%d'),

        datestr2days: function(dstr) {
            let me = this;
            let date = me.parse_date(dstr);
            return (date.getTime() - me.time0) / 1000 / 3600 / 24; // days since time0
        },

        days2datestr_gen: function(fmt = '%b %d') {
            let me = this;
            return function(days) {
                let d = new Date(+days * 24 * 3600 * 1000 + me.time0);
                let dstr = My.date_to_string(d, fmt);
                return dstr;
            }
        },

        load_data: function() {
            let me = this;
            let margin = 1.1;
            let promise = this.loader(me).then(function(res){
                me.legends = res.legends;
                me.colors = res.colors;
                me.opacity = res.opacity;
                me.country = res.country;
                me.stroke_width = res.width;
                me.population = res.population;
                me.dt = res.dt;
                me.date0 = res.date0;
                me.data = res.data;
                me.observed = res.observed;
                me.nplots = me.data[0].vals.length;
                me.xmin = 0.1;
                me.xmax = me.data[me.data.length - 1].time;
                me.xdomain = [me.xmin, me.xmax * margin];
                me.ymin = 0.1;
                me.ymax = me.population;
                me.ydomain = [me.ymin, me.ymax * margin];
                me.y2domain = [0.001, 100.0 * margin];
                me.time0 = Date.parse(res.date0);
            });
            return promise;
        },

        plot_d3: function() {
            let me = this;
            let divid = me.container.str;
            d3.select(divid).select("svg").remove();

            let svg = d3.select(divid).append("svg")
                .attr("width", me.width)
                .attr("height",me.height)
                .append("g")
                .attr("class", "base")
                .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

            me.scale_xy();

            let xfmt = d3.formatSpecifier('d');
            xfmt.comma = true;
            me.xaxis = d3.axisBottom()
                .scale(me.xscale)
                .ticks(5)
                .tickFormat(d3.format(xfmt))

            me.x2axis = d3.axisBottom()
                .scale(me.xscale)
                .ticks(10)
                .tickValues(me.x2tickvals())
                .tickFormat(me.days2datestr_gen())

            let yfmt = d3.formatSpecifier('d');
            yfmt.comma = true;
            me.yaxis = d3.axisLeft()
                .scale(me.yscale)
                .ticks(5)
                .tickFormat(d3.format(yfmt))

            let y2fmt = d3.formatSpecifier('.2f');
            me.y2axis = d3.axisRight()
                .scale(me.y2scale)
                .ticks(5)
                .tickFormat(d3.format(y2fmt))

            function make_y_gridlines() {
                return d3.axisLeft(me.yscale).ticks(5)
            }

            me.line = [];
            for (let p = 0; p < me.nplots; p++) {
                me.line[p] = d3.line()
                    .x(function(d, i) {
                        let x = +d.time;
                        return me.xscale(x);
                    })
                    .y(function(d, i) {
                        let y = +d.vals[p];
                        return me.yscale(y);
                    })
                    .defined(function(d, i) {
                        // if (p == 0) console.log('x:', me.xscale(+d.time), ' y:', me.yscale(+d.vals[p]));
                        return +d.time > 0 && +d.vals[p] > 0;
                    });
            }

            // data plot                                                                                         
            let clippath = svg.append("clipPath")
                .attr("id", "clip0")
                .append("rect")
                .attr("id", "rect0")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", me.width0)
                .attr("height", me.height0)

            for (let j = 0; j < me.nplots; j++) {
                let st = me.colors[j % me.colors.length];
                let op = me.opacity[j % me.opacity.length];
                let sw = me.stroke_width[j % me.stroke_width.length];
                svg.append("g")
                    .attr("clip-path", "url(#clip0)")
                    .append("path")
                    .datum(me.data)
                    .attr("stroke", st)
                    .attr("stroke-width", sw)
                    .attr("opacity", op)
                    .attr("class", "line")
                    .attr("id", "line" + j)
                    .attr("d", me.line[j]);
            }
            svg.append("g")
                .attr("class", "grid")
                .call(make_y_gridlines().tickSize(-me.width0).tickFormat(""))

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0,  " + me.height0 + ")")
                .call(me.xaxis)
                .selectAll("text")
                .style("font-size", me.fontsize1)
                .style("text-anchor", "middle")
                .attr("transform", "rotate(0)");

            svg.append("g")
                .attr("class", "x2 axis")
                .attr("transform", "translate(-20,  " + (me.height0 + 40) + ")")
                .call(me.x2axis)
                .selectAll("text")
                .style("font-size", me.fontsize1)
                .attr("transform", "rotate(-70)")
                .style("text-anchor", "middle")

            svg.append("g")
                .attr("class", "y axis")
                .call(me.yaxis)
                .selectAll("text")
                .style("font-size", me.fontsize1);

            svg.append("g")
                .attr("class", "y2 axis")
                .attr("transform", "translate(" + me.width0 + ", 0)")
                .call(me.y2axis)
                .selectAll("text")
                .style("font-size", me.fontsize1);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "ylabel")
                .attr("x", -me.height0 * 0.6)
                .attr("y", -70)
                .attr("text-anchor", "middle")
                .attr("transform", "rotate(-90)")
                .text(me.ylabel);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "ylabel")
                .attr("x", -me.height0 * 0.6)
                .attr("y", me.width0 + 60)
                .attr("text-anchor", "middle")
                .attr("transform", "rotate(-90)")
                .text(me.y2label);

            svg.append("g")
                .append("text")
                .style("font-size", me.fontsize0)
                .attr("class", "xlabel")
                .attr("x", me.width0 - 40)
                .attr("y",  me.height0 + 20)
                .attr("text-anchor", "middle")
                .text(me.xlabel);

            for (let i = 0; i < me.nplots; i++) {
                let id = 'legend' + i;
                // let x = me.width0 - 450 + 120 * i;
                // let y = me.margin.top + me.height0 - 70;
                svg.append("g")
                    .append("text")
                    .attr("x", 550)
                    // .attr("y", y)
                    .attr("id", id)
                    .attr("class", 'legend')
                    .style("text-anchor", "left")
                    // .style("font-size", '10pt')
                    .style("font-size", me.fontsize1)
                    .attr("stroke", me.colors[i])
                    .attr("fill", me.colors[i])
                    .attr("opacity", me.opacity[i])
                    .text(me.legends[i]);
            }


            // guide line
            svg.selectAll(".guideLine").remove();
            svg.selectAll(".tip").remove();

            d3.select(divid).on("mousemove", function() {
                let area = document.getElementById("chart0");
                let pos = d3.mouse(area);
                let ml = me.margin.left;
                let i = parseInt(me.xscale.invert(pos[0] - ml) / me.dt);
                if (i <= 0 || me.data.length <= i) return;
                let d = me.data[i];
                let v = d.vals;
                let n = me.population;
                guideLine
                    .transition()
                    .duration(10)
                    .attr("x1", pos[0] - ml * 2)
                    .attr("x2", pos[0] - ml * 2)

                let str = 'day ' + parseInt(d.time) + ' (' + me.days2datestr_gen('%b %d, %Y')(parseInt(d.time)) + ')';
                tooltip_date
                    .attr("x", pos[0] - ml + 10)
                    .text(str);
                for (let p = 0; p < me.nplots; p++) {
                    str = My.int2human(parseInt(v[p]), true, 1);
                    str += ' ' + (v[p] / n * 100).toFixed(1) + '%';
                    tooltip[p]
                        .attr("x", pos[0] - ml + 10)
                        .text(str);
                }
            });

            let overLayer = svg.append("g").attr("class", "overLayer")
                .attr("transform", "translate(" + me.margin.left + ", " + me.margin.top + ")");

            let guideLine = overLayer.append("line")
                .attr("class", "guideLine")
                .attr("x1", 0)
                .attr("y1", -me.margin.top + 0)
                .attr("x2", 0)
                .attr("y2", -me.margin.top + me.height0)
                .attr("stroke", "#aaaaaa")

            let tooltip_date = null;
            let tooltip = [];

            tooltip_date = svg.append("g").append("text")
                .style("font-size", "12px")
                .attr("class", "tip")
                .attr("x", 0)
                .attr("y",  me.margin.top - 40 )
                .attr("text-anchor", "left")
                .attr("stroke", '#888888')
                .attr("fill", '#000000')
                .text("");
            for (let p = 0; p < me.nplots; p++) {
                tooltip.push(svg.append("g").append("text")
                             .style("font-size", "12px")
                             .attr("class", "tip")
                             .attr("x", 0)
                             .attr("y",  me.margin.top - 20 + 20 * p)
                             .attr("text-anchor", "left")
                             .attr("stroke", me.colors[p])
                             .attr("fill", me.colors[p])
                             .text(""));
            }

            if (me.observed) {
                for (let p = 0; p < me.nplots; p++) {
                    if (!me.observed.data[p]) continue;
                    let dot = svg.selectAll(".dot" + p)
                        .data(me.observed.data[p])
                        .enter()
                        .append("g")
                        .append("circle")
                        .attr('r', 5)
                        .attr('cx', function(d) {
                            return me.xscale(me.datestr2days(d.date));
                        })
                        .attr('cy', function(d) {
                            let iy = +d.val;
                            if (iy <= 0) iy = 1e-3;
                            return me.yscale(iy);
                        })
                        .attr("class", 'dot' + p)
                        .attr("stroke", me.colors[p])
                        .attr("fill", 'none')
                        .attr("opacity", me.opacity[p])
                }
            }
            else {
                for (let p = 0; p < me.nplots; p++) {
                    svg.selectAll(".dot" + p).remove();
                }
            }
        },


        replot_d3: function() {
            let me = this;
            let divid = me.container.str;
            let svg = d3.select(divid).selectAll(".base");

            me.scale_xy();

            me.xaxis.scale(me.xscale).ticks(5);
            me.x2axis.scale(me.xscale).ticks(10);
            me.yaxis.scale(me.yscale).ticks(5);
            me.y2axis.scale(me.y2scale).ticks(5);

            svg.selectAll("g.x")
                .transition().duration(400)
                .call(me.xaxis)
                .selectAll("text")
                .attr("transform", "rotate(0)")

            me.x2axis.tickValues(me.x2tickvals());

            svg.selectAll("g.x2")
                .transition().duration(400)
                .call(me.x2axis)
                .selectAll("text")
                .attr("transform", "rotate(-70)")

            svg.selectAll("g.x2 path").remove();
            svg.selectAll("g.x2 line").remove();

            svg.selectAll("g.y")
                .transition().duration(400)
                .call(me.yaxis);

            svg.selectAll("g.y2")
                .transition().duration(400)
                .call(me.y2axis);

            for (let p = 0; p < me.nplots; p++) {
                let d = svg.selectAll("path#" + 'line' + p).datum(me.data);
                d.transition().duration(400).attr("d", me.line[p]);
            }

            // a trick to avoid overlapping legends
            let legendy = [];
            for (let p = 0; p < me.nplots; p++) {
                legendy[p] = {
                    p: p,
                    y: me.yscale(me.data[me.data.length - 1].vals[p]),
                }
            }
            legendy = legendy.sort(function(a, b) {
                return b.y - a.y;
            });
            for (let p = 0; p < me.nplots; p++) {
                if (p == me.nplots - 1) continue;
                if (legendy[p].y - 20 < legendy[p + 1].y) {
                    legendy[p + 1].y = legendy[p].y - 20;
                }
            }
            for (let p = 0; p < me.nplots; p++) {
                let p0 = legendy[p].p;
                let d = svg.selectAll("#legend" + p0)
                    .data(me.data).transition().duration(400)
                    .attr("y", function(d) {
                        return legendy[p].y - 10;
                    });
                    
            }

            if (me.observed) {
                for (let p = 0; p < me.nplots; p++) {
                    if (!me.observed.data[p]) continue;
                    let dot = svg.selectAll(".dot" + p)
                        .data(me.observed.data[p])
                        .transition().duration(400)
                        .attr('cx', function(d) {
                            return me.xscale(me.datestr2days(d.date));
                        })
                        .attr('cy', function(d) {
                            let iy = +d.val;
                            if (iy <= 0) iy = me.ymin;
                            return me.yscale(iy);
                        })
                }
            }
            else {
                for (let p = 0; p < me.nplots; p++) {
                    svg.selectAll(".dot" + p).remove();
                }
            }
        },

        scale_xy: function() {
            let me = this;
            let d = me.data;

            let xd = [me.xdomain[0], me.xdomain[1]];
            me.xscale = me.islogx ? d3.scaleLog() : d3.scaleLinear();
            me.xscale.domain(xd).range([0, me.width0]);

            let yd = [me.ydomain[0], me.ydomain[1]];
            me.yscale = me.islogy ? d3.scaleLog() : d3.scaleLinear();
            me.yscale.domain(yd).range([me.height0, me.margin.top]);

            let y2d = [me.y2domain[0], me.y2domain[1]];
            me.y2scale = me.islogy ? d3.scaleLog() : d3.scaleLinear();
            me.y2scale.domain(y2d).range([me.height0, me.margin.top]);
        },

        x2tickvals: function() {
            let me = this;
            let ticks = [];

            let stride = 7;
            let len = me.data[me.data.length - 1].time;
            if (len > 365) {
                stride = 28;
            }
            else if (len > 365 * 0.5) {
                stride = 14;
            }
            if (me.islogx) {
                for (let i in me.data) {
                    let d = me.data[i];
                    if (i != 0 && d.time % stride) continue;
                    if (d.time > 0) ticks.push(d.time);
                }
            }
            else {
                for (let i in me.data) {
                    let d = me.data[i];
                    if (i != 0 && d.time % stride) continue;
                    if (me.xscale(d.time) > me.width0)  continue;
                    ticks.push(d.time);
                }
            }
            return ticks;
        },
    }

    Object.assign(SimChart.prototype, prototype);

}(jQuery);
