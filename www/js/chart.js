"use strict";

let Chart = null;

!function($) {
    Chart = function(containerstr, params) {
        try {
            this.container = document.querySelector(containerstr);
            this.container.className = "chart";
            this.container.str = containerstr;

            // default params                                                                                
            this.margin = { top: 20, right: 20, bottom: 70, left: 60, };
            this.width = 480;
            this.height = 320;
            this.fontsize0 = "16px";
            this.fontsize1 = "10px";
            if (typeof params === 'undefined') {
                throw('Chart(container_id, param) requires param.');
            }
            for (let key in params) {
                this[key] = params[key];
            }
            let required = ['loader', ];
            for (let key of required) {
                if (!this[key]) {
                    throw('Chart(container_id, param) requires param.' + key + '.');
                }
            }

            // init lets                                                                                     
            this.width0 = this.width - this.margin.left - this.margin.right;
            this.height0 = this.height - this.margin.top - this.margin.bottom;
        }
        catch (e) {
            console.log(e);
        }
    };

    Chart.prototype = {
        constructor: Chart,

        set_loader: function(loader) {
            this.loader = loader;
        },

        reload_params: function(params) {
            if (typeof params !== 'undefined') {
                for (let key in params) {
                    this[key] = params[key];
                }
            }
        },

        replot: function(params) {
            this.reload_params(params);
            this.replot_d3();
        },

        new_plot: function() {
            let me = this;
            this.load_data().then(function() {
                me.plot_d3();
                me.replot_d3();
            });
        },

        renew_plot: function() {
            let me = this;
            this.load_data().then(function() {
                me.replot_d3();
            });
        },
    }
}(jQuery);
