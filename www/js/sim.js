!function($) {
    "use strict";

    let PresetList = {
        sample0  : { label: 'Sample0', tag: '',  nbeta: 0.20, gammainv: 14, tmax: 224, n:   100000,  i0:   1, r0:   0, date0: '2020/02/01', },
      hokkaido0: { label: 'Hokkaido0',  tag: 'Hokkaido',  nbeta: 0.04, gammainv: 30, tmax: 365, n: 5281000,  i0:  79, r0: 75, date0: '2020/03/18', },
        tokyo0: { label: 'Tokyo0',  tag: 'Tokyo',  nbeta: 0.16, gammainv: 30, tmax: 365,       n: 13950000,  i0:  92, r0: 25, date0: '2020/03/18', },
        tokyo1: { label: 'Tokyo1',  tag: 'Tokyo',  nbeta: 0.13, gammainv: 30, tmax: 365,       n: 13950000,  i0: 539, r0: 54, date0: '2020/04/01', },
        osaka0: { label: 'Osaka0',  tag: 'Osaka',  nbeta: 0.13, gammainv: 30, tmax: 365,       n:  8821000,  i0:  95, r0: 22, date0: '2020/03/18', },
        hyogo0: { label: 'Hyogo0',  tag: 'Hyogo',  nbeta: 0.10, gammainv: 30, tmax: 365,       n:  5454000,  i0:  87, r0:  3, date0: '2020/03/18', },
        kyoto0: { label: 'Kyoto0',  tag: 'Kyoto',  nbeta: 0.15, gammainv: 30, tmax: 365,       n:  2577000,  i0:  14, r0:  6, date0: '2020/03/18', },
        fukuoka0: { label: 'Fukuoka0',  tag: 'Fukuoka',  nbeta: 0.24, gammainv: 30, tmax: 365, n:  5108000,  i0:   3, r0:  1, date0: '2020/03/18', },
        okinawa0: { label: 'Okinawa0',  tag: 'Okinawa',  nbeta: 0.18, gammainv: 30, tmax: 365, n:  1457000,  i0:   7, r0:  2, date0: '2020/04/01', },
        newyork0: { label: 'NewYorkCity0',  tag: 'New York City',  nbeta: 0.56, gammainv: 30, tmax: 365, n:  8399000,  i0: 12, r0:  0, date0: '2020/03/08', },
        newyork1: { label: 'NewYorkCity1',  tag: 'New York City',  nbeta: 0.12, gammainv: 30, tmax: 365, n:  8399000,  i0: 14904, r0:  0, date0: '2020/03/24', },
        losangeles0: { label: 'LosAngeles0',  tag: 'Los Angeles',  nbeta: 0.26, gammainv: 30, tmax: 365, n:  3990000,  i0: 14, r0:  0, date0: '2020/03/08', },
        losangeles1: { label: 'LosAngeles1',  tag: 'Los Angeles',  nbeta: 0.09, gammainv: 30, tmax: 365, n:  3990000,  i0: 4605, r0:  0, date0: '2020/04/04', },
    };
    let Preset = null;
    let $AdvancedDiv = $('#advanced-div');
    let NParamRow = 1;
    let ChartId = '#chart0';
    let ChartInstance = null;
    let CasesList = [];
    let Cases = null;
    let DefaultXscale = 'linear';
    let DefaultYscale = 'linear';
    let DefaultLang = 'en';
    let IsEnglish = null;
    let SirInstance = null;

    function init_view() {
        // x scaling radio buttons
        let $xsradio = $('#xscale');
        $xsradio.find('.btn[value=' + DefaultXscale + ']').addClass('active');
        $xsradio.find('.btn').click(function() {
            $xsradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            ChartInstance.replot({
                islogx: $(this).attr('value') === 'log',
            });
        });

        // y scaling radio buttons
        let $ysradio = $('#yscale');
        $ysradio.find('.btn[value=' + DefaultYscale + ']').addClass('active');
        $ysradio.find('.btn').click(function() {
            $ysradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            ChartInstance.replot({
                islogy: $(this).attr('value') === 'log',
            });
        });

        // language radio buttons
        let xlabel_jp = '日数';
        let ylabel_jp = '人数';
        let xlabel_en = 'Days';
        let ylabel_en = 'Number of cases';
        let $langradio = $('#lang');
        $langradio.find('.btn[value=' + DefaultLang + ']').addClass('active');
        IsEnglish = DefaultLang === 'en' ? true : false;
        My.switch_lang(IsEnglish);
        $langradio.find('.btn').click(function() {
            $langradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            IsEnglish = $(this).attr('value') === 'en' ? true : false;
            My.switch_lang(IsEnglish);
            ChartInstance.replot({
                xlabel: IsEnglish ? xlabel_en : xlabel_jp,
                ylabel: IsEnglish ? ylabel_en : ylabel_jp,
            });
            ChartInstance.new_plot();
        });

        let $presetsel = $('#preset-sel');
        for (let key in PresetList) {
            let p = PresetList[key];
            $presetsel.append($('<option>').text(p.label).val(key));
        }
        $presetsel.change(function() {
            Preset = PresetList[$(this).val()];
            $('#popu-text').val(Preset.n);
            $('#i0-text').val(Preset.i0);
            $('#r0-text').val(Preset.r0);
            $('#date0-text').val(Preset.date0);
            $('#tmax-text').val(Preset.tmax);
            $('#nbeta-text0').val(Preset.nbeta);
            $('#gammainv-text0').val(Preset.gammainv);
            set_brnum(0);
            if (Preset.tag in CasesList) {
                Cases = CasesList[Preset.tag];
                // console.log(Cases);
            }
            else {
                Cases = null;
            }
            if (ChartInstance) {
                ChartInstance.new_plot();
            }
        });
        $presetsel.val('tokyo0').trigger('change');

        for (let id of ['#popu-text', '#i0-text', '#r0-text', '#date0-text', '#tmax-text']) {
            $(id).change(function() {
                ChartInstance.renew_plot();
            });
        }
        for (let id of ['#nbeta-text0', '#gammainv-text0']) {
            $(id).change(function() {
                set_brnum(0);
                ChartInstance.renew_plot();
            });
        }

        add_dyn_param_head();
        // add_dyn_param_row();


        let dparam = {
            width: 1024,
            height: 512,
            margin: { top: 30, right: 70, bottom: 100, left: 90, },
            loader: loader,
            xlabel: IsEnglish ? xlabel_en : xlabel_jp,
            ylabel: IsEnglish ? ylabel_en : ylabel_jp,
            y2label: '%',
            islogx: DefaultXscale === 'log',
            islogy: DefaultYscale === 'log',
            lang: $('#lang').find('.btn.active').attr('value'),
            ymin: null,
        };

        ChartInstance = new SimChart(ChartId, dparam);
    }

    function set_brnum(row) {
        let b = $('#nbeta-text' + row).val();
        let g = $('#gammainv-text' + row).val();
        $('#brnum-text' + row).val((b * g).toFixed(2));
    }

    function add_dyn_param_head() {
        let row = '';
        row += '<br/>';
        row += '<div class="row">';
        row += '  <div class="col-sm-10" id="dyn-param-div">';
        row += '    <table id="dyn-param-tbl">';
        row += '      <thead>';
        row += '        <tr colspan="5"><span class="en">Change Parameters Dynamically</span></tr>';
        row += '        <tr colspan="5"><span class="jp">パラメタを途中で変える</span></tr>';
        row += '        <tr>';
        row += '          <td>Day</td><td class="param">N &beta;</td><td class="param">1 / &gamma;</td><td class="param">R<sub>0</sub></td>';
        row += '          <td>';
        row += '            <a class="btn btn-default btn-sm radio-selector" value="en" id="add-btn"><span class="en">Add</span><span class="jp">追加</span></a>';
        row += '          </td>';
        row += '          <td>';
        row += '            <a class="btn btn-default btn-sm radio-selector" value="en" id="delete-btn"><span class="en">Delete</span><span class="jp">削除</span></a>';
        row += '          </td>';
        row += '        </tr>';
        row += '      </thead>';
        row += '      <tbody>';
        row += '      </tbody>';
        row += '    </table>';
        row += '  </div>';
        row += '</div>';
        $AdvancedDiv.append(row);
        $('#delete-btn').attr('disabled', true);
        My.switch_lang(IsEnglish);

        $('#add-btn').click(function() {
            add_dyn_param_row();
            $('#delete-btn').attr('disabled', false);
        });

        $('#delete-btn').click(function() {
            if (NParamRow > 1) {
                $('#dyn-param-tbl tbody tr:nth-child(' + (NParamRow - 1) + ')').remove();
                NParamRow -= 1;
                if (NParamRow == 1) $('#delete-btn').attr('disabled', true);
                ChartInstance.renew_plot();
            }
        });
    }

    function add_dyn_param_row() {
        let i = NParamRow;
        let rowdata = {};
        let row = '';
        row += '<tr>';
        row += '<td>';
        row += '<input type="number" id="day-text' + i + '" step="1" min="1"></input>';
        row += '</td>';
        row += '<td>';
        row += '<input type="number" id="nbeta-text' + i + '" step="0.01" min="0"></input>';
        row += '</td>';
        row += '<td>';
        row += '<input type="number" id="gammainv-text' + i + '" step="0.01" min="0"></input>';
        row += '</td>';
        row += '<td>';
        row += '<input type="number" id="brnum-text' + i + '" readonly disabled"></input>';
        row += '</td>';
        row += '<td>';
        row += '</td>';
        row += '<td>';
        row += '</td>';
        row += '</tr>';
        $('#dyn-param-tbl tbody').append(row);

        let i0 = i - 1;
        let day = i == 1 ? 0 : $('#day-text' + i0).val();
        let nbeta = $('#nbeta-text' + i0).val();
        let gammainv = $('#gammainv-text' + i0).val();
        let brnum = nbeta * gammainv;

        $('#day-text' + i).val(day);
        $('#nbeta-text' + i).val(nbeta);
        $('#gammainv-text' + i ).val(gammainv);
        $('#brnum-text' + i).val(brnum);

        for (let id of [ '#day-text' + i, '#nbeta-text' + i, '#gammainv-text' + i]) {
            $(id).change(function() {
                set_brnum(i);
                ChartInstance.renew_plot();
            });
        }

        NParamRow += 1;
    }

    function loader() {
        let $dfd = new $.Deferred();
        let tmax = $('#tmax-text').val();
        let n = $('#popu-text').val();
        let i0 = $('#i0-text').val();
        let r0 = $('#r0-text').val();
        let t = 0;
        let dt = 0.5;
        let sir = null;

        for (let turn = 0; turn < NParamRow; turn++) {
            let nbeta = $('#nbeta-text' + turn).val();
            let gammainv = $('#gammainv-text' + turn).val();
            let model_param = {
                n: n,
                s: 1 - i0 / n - r0 / n,
                i: i0 / n,
                r: r0 / n,
                nbeta: nbeta,
                gammainv: gammainv,
                dt: dt,
            };
            if (!sir) {
                sir = SirInstance = new SirModel(model_param);
                sir.init();
            }
            else {
                sir.reload_params({ nbeta: nbeta, gammainv: gammainv, });
            }
            let tend = turn == (NParamRow - 1) ? tmax : +$('#day-text' + (turn + 1)).val();
            for (; t < tend; t += dt) {
                sir.integrate();
            }
        }

        let res = sir.report();

        let data = [];
        for (let step of res.step) {
            data.push({
                time: step.t,
                vals: [
                    step.s * n,
                    step.i * n,
                    step.r * n,
                ],
            });
        }

        let legend_en = [ 'Susceptible',
                          'Infected',
                          'Recovered (assume perfect immunity)',
                        ];

        let legend_jp = [ '未感染 (感受性保持者)',
                          '感染中 (感染者)',
                          '回復者 (免疫保持者と仮定)',
                        ];
        let dst = {
            legends: IsEnglish ? legend_en: legend_jp ,
            colors: [ '#22cc66', '#ee4444', '#4444ff', '#88ff44', '#ee8844', ],
            opacity: [ 1.0, 1.0, 1.0, 1.0],
            width: [ 2.0, 2.0, 2.0, 2.0],
            population: n,
            dt: dt,
            date0: $('#date0-text').val(),
            data: data,
            observed: Cases,
        };
        $dfd.resolve(dst);
        return $dfd.promise();
    }

    $(function init() {
        window.My = new MyUtil();
        let p = My.get_url_param('lang');
        if (p) DefaultLang = p;
        My.init_model('tk').then(function(raw) {
            CasesList = My.raw2analyzed_jp_tk(raw);
            return My.init_model('csse', 0);
        }).then(function(raw) {
            let res = My.raw2analyzed_us_csse(raw);
            Object.assign(CasesList, res);
            console.log(CasesList);
            init_view();
        });
    });

}(jQuery);
