!function($) {
    "use strict";

    let DataType = 0; // 0: cases 1: deaths
    let ChartId = '#chart0';
    let ChartInstance = null;
    let Cases = [];
    let DefaultXscale = 'log';
    let DefaultYscale = 'log';
    let DefaultLang = 'en';
    let IsEnglish = null;

    function init_view() {
        // x scaling radio buttons
        let $xsradio = $('#xscale');
        $xsradio.find('.btn[value=' + DefaultXscale + ']').addClass('active');
        $xsradio.find('.btn').click(function() {
            $xsradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            ChartInstance.replot({
                islogx: $(this).attr('value') === 'log',
            });
        });

        // y scaling radio buttons
        let $ysradio = $('#yscale');
        $ysradio.find('.btn[value=' + DefaultYscale + ']').addClass('active');
        $ysradio.find('.btn').click(function() {
            $ysradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            ChartInstance.replot({
                islogy: $(this).attr('value') === 'log',
            });
        });

        // language radio buttons
        let xlabel_jp = 'N: 人口 1 億人あたりの罹患者数';
        let ylabel_jp = 'N の 1 日あたりの増分';
        let xlabel_en = 'N := number of cases per 100 million';
        let ylabel_en = 'Increase of N per day';
        let $langradio = $('#lang');
        $langradio.find('.btn[value=' + DefaultLang + ']').addClass('active');
        IsEnglish = DefaultLang === 'en' ? true : false;
        My.switch_lang(IsEnglish);
        $langradio.find('.btn').click(function() {
            $langradio.find('.btn').removeClass('active');
            $(this).addClass('active');
            IsEnglish = $(this).attr('value') === 'en' ? true : false;
            My.switch_lang(IsEnglish);
            ChartInstance.replot({
                xlabel: IsEnglish ? xlabel_en : xlabel_jp,
                ylabel: IsEnglish ? ylabel_en : ylabel_jp,
            });
            ChartInstance.new_plot();
        });
        // reference countries
        for (let sel of [ '#ref0-sel', '#ref1-sel', '#ref2-sel', ]) {
            for (let c in My.country) {
                if (c === 'Japan') continue;
                let text = c;
                $(sel).append($("<option>").text(text).val(c))
            }
            $(sel).change(function() {
                ChartInstance.new_plot();
            });
        }
        $('#ref0-sel').val('World w/o China');
        $('#ref1-sel').val('China');
        $('#ref2-sel').val('United States');

        let dparam = {
            width: 768,
            height: 768,
            margin: { top: 50, right: 50, bottom: 50, left: 70, },
            loader: loader,
            xlabel: IsEnglish ? xlabel_en : xlabel_jp,
            ylabel: IsEnglish ? ylabel_en : ylabel_jp,
            islogx: DefaultXscale === 'log',
            islogy: DefaultYscale === 'log',
            lang: $('#lang').find('.btn.active').attr('value'),
        };
        ChartInstance = new NvsdnChart(ChartId, dparam);
    }

    function loader() {
        let $dfd = new $.Deferred();
        let data = [];
        let c0 = 0, c1 = 0, c2 = 0, c3 = 0;
        for (let c of Cases) {
            if (!c.date) continue;
            var ref0 = $('#ref0-sel').val();
            var ref1 = $('#ref1-sel').val();
            var ref2 = $('#ref2-sel').val();
            data.push({
                date: c.date.replace(/-/g, '/'),
                xvals: [
                    c['Japan'],
                    c[ref0],
                    c[ref1],
                    c[ref2],
                ],
                yvals: [
                    c['Japan']-c0,
                    c[ref0]-c1,
                    c[ref1]-c2,
                    c[ref2]-c3,
                ],
            })
            c0 = c['Japan'];
            c1 = c[ref0];
            c2 = c[ref1];
            c3 = c[ref2];
        }
        let ctr = My.country;
        let legend_en = [ 'Japan',
                          ref0,
                          ref1,
                          ref2,
                        ];

        let legend_jp = [ '日本',
                          ctr[ref0].jp,
                          ctr[ref1].jp,
                          ctr[ref2].jp,
                        ];
        let dst = {
            legends: IsEnglish ? legend_en: legend_jp ,
            colors: [ '#4444ff', '#44ccff', '#88ff44', '#ccdd44', '#ee8844', ],
            opacity: [ 1.0, 1.0, 1.0, 1.0],
            width: [ 2.0, 2.0, 2.0, 2.0],
            country: [ctr.Japan, ctr[ref0], ctr[ref1], ctr[ref2], ],
            data: data,
        };
        $dfd.resolve(dst);
        return $dfd.promise();
    }

    $(function init() {
        window.My = new MyUtil();
        let p = My.get_url_param('lang');
        if (p) DefaultLang = p;
        My.init_model('ow', DataType).then(function(raw) {
            Cases = My.raw2analyzed_ow(raw, 0);
            init_view();
        });
        let timer = false;
        window.addEventListener('resize', function() {
            if (timer !== false) {
                clearTimeout(timer);
            }
            timer = setTimeout(function() {
                let c = ChartInstance;
                let w = $('#chart0').width();
                let m = c.margin;
                m.left = 512 < w ? 100 : 40;
                c.replot({ width: w, margin : m, });
            }, 1000);
        });

    });

}(jQuery);
