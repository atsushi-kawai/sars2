!function($) {
    "use strict";

    let MyUtil = function() {
        try {
            if ("undefined"== typeof document) return false;
            this.mega = 1024 * 1024;
            this.giga = 1024 * 1024 * 1024;
        }
        catch (e) {
            console.log(e);
        }
    };

    MyUtil.prototype = {

        data_src: {
            'tk': 'https://raw.githubusercontent.com/kaz-ogiwara/covid19/master/data/data.json',
            'ow': [
                'https://covid.ourworldindata.org/data/ecdc/total_cases.csv',
                'https://covid.ourworldindata.org/data/ecdc/total_deaths.csv',
            ],
            'csse': [
                'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv',
                'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv',
            ],
        },

        country : {
            Japan:         { jp: '日本',         offset: [ '0', '0'], population:  125950000, },
            World:         { jp: '全世界',       offset: ['-19', '-32'], population: 7715000000, },
            'World w/o China':         { jp: '中国を除く全世界',       offset: ['+8', '-2'], population: 7715000000 - 1386050000, },
            China:         { jp: '中国',         offset: ['-24', '-42'], population: 1386050000, },
            France:        { jp: 'フランス',     offset: ['+12', '-5'], population:   66990000, },
            Germany:       { jp: 'ドイツ',       offset: ['+12', '+4'], population:   82790000, },
            India:         { jp: 'インド',       offset: ['+37', '+37'], population: 1352610000, },
            Italy:         { jp: 'イタリア',     offset: [ '+8', '-15'], population:   60480000, },
            Philippines:   { jp: 'フィリピン',   offset: ['+25', '+5'], population:  104900000, },
            Russia:        { jp: 'ロシア',       offset: ['+29', '+21'], population:  144500000, },
            Spain:         { jp: 'スペイン',     offset: ['+13', '-3'], population:   46660000, },
            'South Korea': { jp: '韓国',         offset: ['-13', '-17'], population:   51470000, },
            Taiwan:        { jp: '台湾',         offset: ['-17', '+15'], population:   23780000, },
            'United States': { jp: 'アメリカ',   offset: ['+16', '+1'], population:  327200000, },
        },

        init_model: function(src, type = null) {
            let $dfd = $.Deferred();
            // load data
            switch (src) {
            case 'tk':
                $.get(My.data_src[src]).then(function(res){
                    res = JSON.parse(res);
                    $dfd.resolve(res);
                }, function(e) {
                    console.log('error:', e);
                });
                break;
            case 'ow':
                $.get(My.data_src[src][type]).then(function(res){
                    $dfd.resolve(res);
                }, function(e) {
                    console.log('error:', e);
                });
                break;
            case 'csse':
                $.get(My.data_src[src][type]).then(function(res){
                    $dfd.resolve(res);
                }, function(e) {
                    console.log('error:', e);
                });
                break;
            }
            return $dfd.promise();
        },

        raw2analyzed_ow: function(raw, extra_days=0) {
            let lines = raw.split(/\r?\n/);
            let header = lines.shift().split(/,/);
            let days = [];
            for (let j in lines) {
                let line = lines[j];
                let fs = line.split(/,/);
                let day = {};
                let i = 0;
                for (let col of header) {
                    day[col] = fs[i];
                    i += 1;
                }
                days.push(day);
            }

            for (let day of days) {
                day['World w/o China'] = (day['World'] - day['China']).toFixed();
            }
            // add future dates with empty data to plot a bit more than the current date
            let lastday = days[days.length-2];
            let d = new Date(lastday.date);
            for (let i = 0; i < extra_days; i++) {
                d.setDate(d.getDate() + 1);
                let dstr = My.date_to_string(d, '%Y/%m/%d');
                let day = { 'date': dstr, };
                days.push(day);
            }
            return days;
        },

        raw2analyzed_tk: function(raw) {
            let srcs = [
                { name: 'carriers', data: raw.transition.carriers, } ,
                { name: 'discharged', data: raw.transition.discharged, } ,
            ];
            let ret = {
                'updateat': raw.updated.last.en,
            };
            for (let src of srcs) {
                let dst = [];
                for (let c of src.data) {
                    let ymd = c[0] + '/' + ('0' + c[1]).slice(-2) + '/' + ('0' + c[2]).slice(-2);
                    dst.push({
                        date: ymd,
                        vals: [ c[3], ],
                    });
                }
                ret[src.name] = dst;
            }
            return ret;
        },

        raw2analyzed_jp_tk: function(raw) {
            let ret = {};
            let prefmap = {};
            for (let pref of raw['prefectures-map']) {
                pref.code += 2; // ?? need to make sure
                prefmap[pref.en] = pref;
            }
            let prefdata = raw['prefectures-data'];

            for (let key in prefmap) { // for each prefecture
                // for (let key of ['Tokyo', 'Hokkaido', 'Okinawa', 'Osaka', 'Hyogo', 'Kyoto', 'Fukuoka']) { // for each prefecture
                let pref = prefmap[key];
                let data = [ null, ]; // no datga for susceptible, of course
                // create lists for accumulated num of carriers, deaths, discharged.
                let sums = {
                    carriers: [],
                    deaths: [],
                    discharged: [],
                };
                for (let type of ['carriers', 'deaths', 'discharged', ]) {
                    let sum = 0;
                    for (let step of prefdata[type]) {
                        let date = step[0] + '/' + step[1] + '/' + step[2];
                        sum = step[pref.code];
                        // console.log('date:', date, ' type:', type, ' sum:', sum);
                        sums[type].push({ date: date, sum: sum, });
                    }
                }
                // console.log(key, ' ', sums);
                // create lists for infected and recovered by subtracting deaths & discharged from carriers.
                let infected = [], recovered = [], carriers = [];
                let valI = 0, valR = 0, valC = 0;
                for (let step of sums.carriers) {
                    let deaths = search_rec_on(step.date, sums.deaths);
                    let discharged = search_rec_on(step.date, sums.discharged);
                    if (!deaths || !discharged) continue;
                    valR = deaths.sum + discharged.sum;
                    valI = step.sum - valR;
                    valC = step.sum;
                    infected.push({
                        date: step.date,
                        val: valI,
                    });
                    recovered.push({
                        date: step.date,
                        val: valR,
                    });
                    carriers.push({
                        date: step.date,
                        val: valC,
                    })
                }
                ret[key] = {
                    name: pref,
                    data: [ null, infected, recovered, carriers, ], // carriers not used. just for debuging purpose.
                };
            }
            // console.log(ret);
            return ret;
            function search_rec_on(datestr, recs) {
                let recfound = null;
                for (let rec of recs) {
                    if (rec.date === datestr) recfound = rec;
                }
                return recfound;
            }
        },

        raw2analyzed_us_csse: function(raw, extra_days=0) {
            let lines = raw.split(/\r?\n/);
            let header = lines.shift().split(/,/);
            let areas = {};
            let provences = ['New York City', 'Los Angeles', ];
            for (let prov of provences) {
                for (let line of lines) {
                    let re = new RegExp(prov);
                    if (!line.match(re)) continue;
                    line = line.replace(/"(.*),(.*),(.*)"/, '$1-$2-$3') // remove commas inside "" of column Combined_Key
                    let fs = line.split(/,/);

                    let days = []
                    let i = 0;
                    for (let col of header) {
                        if (i < 11) {
                            i += 1;
                            continue;
                        }
                        let m = col.match(/(\d+)\/(\d+)\/(\d+)/);
                        let datestr = '20' + m[3] + '/' + ('0' + m[1]).slice(-2) + '/' + m[2];
                        days.push({
                            date: datestr,
                            val: +fs[i],
                        });
                        i += 1;
                    }
                    if (!(prov in areas)) {
                        areas[prov] = {
                            name: { en: prov, jp: prov },
                            data: [ null, days, ],
                        }
                    }
                    else {
                        areas[prov].data[1].map(function(dayi, i){
                            dayi.val += days[i].val;
                            // if (i == 95) console.log('dayi.val:', dayi.val);
                        });
                    }
                }
            }
            return areas;
        },

        date_to_string: function(date, format) {
            let monthname = [
                'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
            ];
            let dayofweekname = [
                'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
            ];

            if (!format) format = '%Y/%m/%d %H:%M:%S';

            format = format.replace(/%Y/g, date.getFullYear());
            format = format.replace(/%m/g, ('0' + (date.getMonth() + 1)).slice(-2));
            format = format.replace(/%d/g, ('0' + date.getDate()).slice(-2));
            format = format.replace(/%H/g, ('0' + date.getHours()).slice(-2));
            format = format.replace(/%M/g, ('0' + date.getMinutes()).slice(-2));
            format = format.replace(/%S/g, ('0' + date.getSeconds()).slice(-2));
            format = format.replace(/%b/g, monthname[date.getMonth()]);
            format = format.replace(/%a/g, dayofweekname[date.getDay()]);
            return format;
        },

        // truncate src to the nearest dsrc aligned value smaller than src.
        align_floor: function(src, dsrc) {
            let dst = (Math.floor(src / dsrc)) * dsrc;
            //    console.log("src:" + src + " dst:" + dst + " dsrc:" + dsrc);
            return dst;
        },

        // round up src to the nearest dsrc aligned value larger than src.
        align_ceil: function(src, dsrc) {
            let dst = (Math.ceil(src / dsrc)) * dsrc;
            //    console.log("src:" + src + " dst:" + dst);
            return dst;
        },

        get_url_param: function(pname) {
            let url = window.location.search;
            pname = pname.replace(/[\[\]]/g, "\\$&");
            let regex = new RegExp("[?&]" + pname + "(=([^&#]*)|&|#|$)");
            let ret = regex.exec(url);
            if (!ret) return null; // not given
            if (!ret[2]) return ''; // no value given
            return decodeURIComponent(ret[2].replace(/\+/g, " "));
        },

        set_lang: function(lang) {
            let links = [];
            $('.navbar-nav a').each(function(i, elem) {
                if ($(elem).attr('id') !== 'undefined') {
                    links.push($(elem));
                }
            });
            for (let i in links) {
                let href = links[i].attr('href');
                if (!href) continue;
                if (href.match(/lang=\w\w/)) {
                    href = href.replace(/lang=\w\w/, 'lang=' + lang)
                }
                else {
                    href += '?lang=' + lang;
                }
                links[i].attr('href', href);
            }
        },

        switch_lang: function(is_en) {
            if (is_en) {
                this.set_lang('en');
                $('.en').show();
                $('.jp').hide();
            }
            else {
                this.set_lang('jp');
                $('.en').hide();
                $('.jp').show();
            }

        },

        int2human: function(num, join, belowbin) {
            if (!belowbin) belowbin = 0; // digits below binary point
            let suf = ['', 'k', 'M', 'B', 'T', ];
            let isint = Number.isInteger(num);
            let i = 0;
            while (num >= 1024 && i < suf.length - 1) {
                num /= 1024.0;
                i += 1;
            }
            let ret;
            if (join) {
                if (isNaN(num) || num == null) {
                    ret = null;
                }
                else {
                    if (i == 0 && Number.isInteger(num)) {
                        ret = +num; // no digits below binary point if num is int and no suffix
                    }
                    else {
                        ret = (+num).toFixed(belowbin) + suf[i];
                    }
                }
            }
            else {
                ret = { "val": num, "suf": suf[i] };
            }
            return ret;
        },
    }
    window.MyUtil = MyUtil;
}(jQuery)
